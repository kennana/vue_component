import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/scroll',
      name: 'scroll',
      component: () => import('./views/Index.vue')
    },
    {
      path: '/pagination',
      name: 'pagination',
      component: () => import('./views/Pagination.vue')
    },
    {
      path: '/mypagination',
      name: 'mypagination',
      component: () => import('./views/Mypagination.vue')
    },
    {
      path: '/menu',
      name: 'menu',
      component: () => import('./views/Menu.vue')
    },
    {
      path: '/navbar',
      name: 'navbar',
      component: () => import('./views/NavBar.vue'),
      children: [
        {
          path: '/home',
          name: 'home-1',
          component: () => import('./views/Home.vue')
        },
        {
          path: '/shares',
          name: 'shares',
          component: () => import('./views/Home.vue')
        },
        {
          path: '/community',
          name: 'community',
          component: () => import('./views/Home.vue')
        },
        {
          path: '/mine',
          name: 'mine',
          component: () => import('./views/Home.vue')
        },
      ]
    },
  ]
})
