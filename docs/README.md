---
home: true
heroImage: https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1547820606078&di=89a7ee299189bbc1725942f867038d89&imgtype=0&src=http%3A%2F%2Fphoto.16pic.com%2F00%2F47%2F68%2F16pic_4768928_b.jpg
actionText: 快速上手 →
actionLink: /zh/guide/
features:
- title: 积累
  details: 平时的一些积累，一些上手即用的东西/知识点等内容，范围较广。
- title: 算法
  details: 刷算法题，是提升编程能力和逻辑能力比较有效的一种方式。文档将算法按照难度分级，代码中都有详细注释，且会提供多种解法。
- title: 持续更新
  details: 本文档将会持续更新，就像我的博客一样，当做一个长期项目来维护，期望这个文档在不久以后能够给大家带来帮助。
footer: MIT Licensed | Copyright © 2018-present Ken_Coding
---
